//
//  AppDelegate.h
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-03.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

