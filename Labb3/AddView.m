//
//  AddView.m
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-04.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "AddView.h"
#import "Task.h"

@interface AddView ()
@property (weak, nonatomic) IBOutlet UITextField *taskName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *taskPriority;

@end

@implementation AddView


- (IBAction)addPressed:(id)sender {
    NSString *priority;
    if (self.taskPriority.selectedSegmentIndex == 0) {
        priority = @"Priority: A";
    } else if (self.taskPriority.selectedSegmentIndex == 1) {
        priority = @"Priority: B";
    } else {
        priority = @"Priority: C";
    }
    Task *task = [[Task alloc] initWithDescription:self.taskName.text andImportance:priority];
    NSLog(@"%d", self.tasks.count);
    [self.tasks addObject:task];
    NSLog(@"%d", self.tasks.count);
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.taskPriority.selectedSegmentIndex = 1;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
