//
//  Task.h
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-03.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property (nonatomic, retain) NSString *taskText;
@property (nonatomic) NSString *importance;

-(id)initWithDescription:(NSString *)description andImportance:(NSString *)importance;

@end
