//
//  AddView.h
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-04.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddView : UIViewController

@property (nonatomic) NSMutableArray *tasks;

@end
