//
//  TodoTable.m
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-03.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "TodoTable.h"
#import "Task.h"
#import "AddView.h"

@interface TodoTable ()

@property (nonatomic) NSMutableArray *tasks;

@end

@implementation TodoTable


- (NSMutableArray*)tasks {
    if (!_tasks) {
        Task *task1 =[[Task alloc] initWithDescription:@"Task1" andImportance:@"Priority: B"];
        Task *task2 =[[Task alloc] initWithDescription:@"Task2" andImportance:@"Priority: C"];
        Task *task3 =[[Task alloc] initWithDescription:@"Task3" andImportance:@"Priority: A"];
        _tasks = [[NSMutableArray alloc] initWithObjects:task1, task2, task3, nil];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"importance" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _tasks = [[_tasks sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    return _tasks;
}

- (IBAction)CompleteTask:(id)sender
{
    UIButton *completeButton = (UIButton*) sender;
    Task *task = self.tasks[completeButton.tag];
    task.importance = @"Priority: Done";
    [self viewWillAppear:NO];
}


- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    NSLog(@"%d", self.tasks.count);
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    NSData *encodedTasks = [NSKeyedArchiver archivedDataWithRootObject:_tasks];
    [userDefault setObject:encodedTasks forKey:[NSString stringWithFormat:@"tasks"]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
    NSData *encodedTasks = [userDefault objectForKey:[NSString stringWithFormat:@"tasks"]];
    _tasks =[[NSKeyedUnarchiver unarchiveObjectWithData: encodedTasks] mutableCopy];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.tasks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyCell"];
    
    // Configure the cell...
    
    NSLog(@"%d", indexPath.row);
    Task *task = self.tasks[indexPath.row];
    cell.textLabel.text = task.taskText;
    cell.detailTextLabel.text = task.importance;
    if (![task.importance isEqualToString:@"Priority: Done"]) {
        UIButton *completeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        completeButton.frame = CGRectMake(220, 10, 70, 30);
        completeButton.tag = indexPath.row;
        [completeButton setTitle:@"Complete" forState:UIControlStateNormal];
        [cell addSubview:completeButton];
        [completeButton addTarget:self action:@selector(CompleteTask:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"Add"]) {
        //DetailView *detailView = [segue destinationViewController];
        //UITableViewCell *cell = sender;
        //detailView.title = cell.textLabel.text;
        AddView *addView = [segue destinationViewController];
        
        addView.tasks = self.tasks;
        NSLog(@"Hej");
        
        
        
    } else {
        NSLog(@"You forgot the segue %@", segue);
    }
}


@end
























