//
//  Task.m
//  Labb3
//
//  Created by IT-Högskolan on 2015-02-03.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "Task.h"
@interface Task()


@end

@implementation Task

-(id)initWithDescription:(NSString *)taskText andImportance:(NSString *)importance {
    if(self = [super init]) {
        
        NSLog(@"Debug response");
        
        // Do something with params
        self.taskText = taskText;
        self.importance = importance;
    }
    
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.taskText forKey:@"taskText"];
    [encoder encodeObject:self.importance forKey:@"importance"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        self.taskText = [decoder decodeObjectForKey:@"taskText"];
        self.importance = [decoder decodeObjectForKey:@"importance"];
        
    }
    return self;
}

@end
